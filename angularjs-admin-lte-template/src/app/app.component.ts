/**
 * Angular 2 decorators and services
 */
import {
  Component,
  OnInit,
  ViewEncapsulation
} from '@angular/core';
import { AppState } from './app.service';
import { User, MenuService, Message, MessagesService, NotificationsService, Notification,LogoService,FooterService } from './ngx-admin-lte';
/**
 * App Component
 * Top Level Component
 */
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: [
    './app.component.css'
  ]
})
export class AppComponent implements OnInit {
  public angularclassLogo = 'assets/img/angularclass-avatar.png';
  public name = 'Admin-Lte Demo';
  public url = 'https://twitter.com/AngularClass';
  private footer = {
    left_part: `<strong>
      Copyright &copy; 2017
      <a href="#" >Lee</a>.
    </strong>
    Open-source Sharing`,
    right_part: 'Admin-Lte-Angularjs2-Template',
  };
  private logo = {
    html_mini: 'NG<b>X</b>',
    html_lg: '<b>NGX</b>Admin-LTE',
  };
  private mylinks: any = [
    {
      'title': 'Home',
      'icon': 'dashboard',
      'link': ['/']
    },
    {
      'title': 'Hero List',
      'icon': 'user',
      'link': ['/hero']
    },
    {
      'title': 'Sub menu',
      'icon': 'link',
      'sublinks': [
        {
          'title': 'Page 2',
          'link': ['/page/2'],
        },
        {
          'title': 'Page 3',
          'link': ['/page/3'],
        }
      ]
    },
  ];
  
  constructor(
    private footerServ: FooterService,
    public appState: AppState,
    private msgServ: MessagesService,
    private logoServ: LogoService,
    private menuServ:MenuService
  ) {}

  public ngOnInit() {
    window.dispatchEvent( new Event( 'resize' ) );
    this.footerServ.setCurrent(this.footer);
    this.logoServ.setCurrent(this.logo);
    this.menuServ.setCurrentMenu(this.mylinks);
    const user1 = new User( {
      avatarUrl: 'assets/img/user2-160x160.jpg',
      email: 'weber.antoine.pro@gmail.com',
      firstname: 'WEBER',
      lastname: 'Antoine'
  });
    console.log('Init App');
    this.msgServ.addMessage( new Message( {
      author: user1,
      content: 'le contenu d\'un message d\'une importance extreme',
      destination: user1,
      title: 'un message super important'
  }) );

  }

}

/**
 * Please review the https://github.com/AngularClass/angular2-examples/ repo for
 * more angular app examples that you may copy/paste
 * (The examples may not be updated as quickly. Please open an issue on github for us to update it)
 * For help or questions please contact us at @AngularClass on twitter
 * or our chat on Slack at https://AngularClass.com/slack-join
 */
