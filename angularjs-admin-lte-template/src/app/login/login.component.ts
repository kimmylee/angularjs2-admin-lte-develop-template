import { Component, OnInit, OnDestroy } from '@angular/core';
import { User, UserService } from '../ngx-admin-lte';
import { Router } from '@angular/router';
import {Http,Headers} from '@angular/http'

@Component({
  selector: 'app-login',
  styles: ['./login.css'],
  templateUrl: './login.component.html'
})
export class LoginComponent implements OnInit {
  public password: string;
  public email: string;

  constructor(
    private userServ: UserService,
    private router: Router,
    private http:Http
  ) {
  }

  public ngOnInit() {
    window.dispatchEvent( new Event( 'resize' ) );
  }

  private login() {

    if(this.email!="" &&this.password!= "")
    {
      var creds="grant_type="+"&username="+this.email+"&password="+this.password+"&refresh_token=";
      var headers= new Headers();
      headers.append('Content-Type', 'application/x-www-form-urlencoded');
        this.http.post('/api/token?'+creds,'',{headers:headers})
        .map(res=>res.json())
        .subscribe(
          data=>{
            localStorage.setItem('access_token',data.tokenData.access_token);
            var user = new User({
              avatarUrl: 'assets/img/user2-160x160.jpg',
              email: data.email,
              firstname: 'WEBER',
              lastname: 'Antoine',
            })
            user.connected=true;
            this.userServ.setCurrentUser(user);
            this.router.navigate( ['home'] );
          },
          err=>console.log(err),
        )
    }
    // // test les champs en js

    // // envoyer les champs a php

    // // si retour positif, log le user
    // if ( 1 === 1 ) {

    //   const user1 = new User( {
    //       avatarUrl: 'assets/img/user2-160x160.jpg',
    //       email: 'weber.antoine@outlook.com',
    //       firstname: 'WEBER',
    //       lastname: 'Antoine'
    //   } );
    //   user1.connected = true;

    //   this.userServ.setCurrentUser( user1 );

    //   this.router.navigate( ['home'] );
    // } else {
    //   // je recupere l'erreur du php
    //   // et on le place dans un label, ou un toaster
    // }
  }

}
