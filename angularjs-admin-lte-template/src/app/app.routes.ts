import { ModuleWithProviders } from '@angular/core';
import { Routes , RouterModule} from '@angular/router';
import { HomeComponent } from './home';
import { LoginComponent } from './login/login.component';
import {RegisterComponent} from './register/register.component'
import { NoContentComponent } from './no-content';
import { DataResolver } from './app.resolver';
import {CanActivateGuard,LayoutAuthComponent, LayoutLoginComponent, LayoutRegisterComponent } from './ngx-admin-lte';

import {HeroListComponent} from './HeroDemo/heros/herolist.component'

const routes: Routes = [
{
    canActivate:[CanActivateGuard],
    children:[
      {
        canActivate:[CanActivateGuard],
        component:HomeComponent,
        path:''
      },
      {
        canActivate: [CanActivateGuard],
        component: HomeComponent,
        path: 'home'
      }
      ,
      {
        canActivate: [CanActivateGuard],
        component: HeroListComponent,
        path: 'hero'
      }
    ],
    component:LayoutAuthComponent,
    data: [{
      'skin': 'skin-blue',
      /*USE THIS IS YOU WANT TO HIDE SOME TEMPLATE PART*/
      'display_tasks': true,
      'display_control': false,
      'display_user': true,
      'display_messages': true,
      'display_notifications': false,
      'display_menu_user': true,
      'display_menu_search': false,
      'menu_title': 'MENU TITLE',
      'display_logout': false
    }],
    path:''
  },
    //not logged routes
    {
      children: [
        {
          component: LoginComponent,
          path: ''
        }
      ],
      component: LayoutLoginComponent,
      path: 'login',
    },
    {
      children: [
        {
          component: RegisterComponent,
          path: ''
        }
      ],
      component: LayoutRegisterComponent,
      path: 'register',
    }
];
export const routing: ModuleWithProviders = RouterModule.forRoot(routes);
