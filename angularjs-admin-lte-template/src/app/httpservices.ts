import {Injectable} from '@angular/core'
import {Http,Headers} from '@angular/http'
@Injectable()
export class HttpServices{
    private commonHeaders= new Headers({'Content-Type': 'application/json'});
    constructor(private http:Http){}  
    createAuthorizationHeader(headers:Headers){
      var accesstoken= localStorage.getItem('access_token');
      console.log('curToken',accesstoken);
        headers.append('Authorization','Bearer  '+accesstoken);
    }
    get(url) {
        let headers = this.commonHeaders;
        this.createAuthorizationHeader(headers);

        return this.http.get(url, {
          headers: headers
        });
      }
      post(url, data) {
        let headers = this.commonHeaders;
        this.createAuthorizationHeader(headers);
        return this.http.post(url, data, {
          headers: headers
        });
      }
      handleError(error: any)
      {
        console.error('An error occurred', error); // for demo purposes only
        return Promise.reject(error.message || error);
      }
}