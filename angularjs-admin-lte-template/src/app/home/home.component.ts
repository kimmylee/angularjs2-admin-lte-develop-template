
import { Component, OnInit, OnDestroy } from '@angular/core';
import { BreadcrumbService , Message , MessagesService , User } from '../ngx-admin-lte';

import {HttpServices} from '../httpservices'

@Component({
  selector: 'app-home',
  styleUrls: ['./home.component.css'],
  templateUrl: './home.component.html'
})
export class HomeComponent implements OnInit, OnDestroy {
  public date: Date = new Date();
  public name: string;
  public age: string;
  constructor(
    private msgServ: MessagesService,
    private breadServ: BreadcrumbService,
    private httpServices:HttpServices
  ) {
    // TODO
  }

  public ngOnInit() {
    // setttings the header for the home
    this.breadServ.set({
      description: 'HomePage',
      display: true,
      header: 'Dashboard',
      levels: [
        {
          icon: 'dashboard',
          link: ['/'],
          title: 'Home'
        }
      ]
    });

  }

  private callApi() { 
        this.httpServices.get('/api/values').map(res=>res.json()).subscribe(data=>{
         this.name=data.name;
         this.age=data.age;
        })
      }

  public ngOnDestroy() {
    // removing the header
    this.breadServ.clear();
  }

}
