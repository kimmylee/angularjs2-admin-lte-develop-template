import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Hero } from '../hero';
import { HttpServices } from '../../httpservices';


@Component({
    selector:'hero-list',
    templateUrl:'./herolist.component.html',
    styleUrls:['./herolist.component.css']
})

export class HeroListComponent implements OnInit{
    herolist:Hero[];
    selectedHero : Hero;
    topHeroes:Hero[];
    constructor(private router:Router,private http:HttpServices){}
    ngOnInit(): void{
        this.getHeros();
    }
    getHeros():void{
        this.http.get('/api/hero').map(res=>res.json())
        .subscribe(data=>{
            this.herolist= data as Hero[];
            this.topHeroes = this.herolist.slice(1,3);
        })
    }
    onSelect(hero:Hero):void{
        this.selectedHero=hero;
    }
    gotoDetail():void{
        this.router.navigate(['/detail',this.selectedHero.id]);
    }

    add(name:string):void{
        this.http.post('/api/hero?name='+name,'').map(res=>res.json())
        .subscribe(data=>{
            var hero= data as Hero;
            this.herolist.push(hero);
            this.selectedHero=null;
        })
    }
}