import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { User, UserService } from '../ngx-admin-lte';
@Injectable()
export class AuthGuard implements CanActivate {

    constructor(private router: Router,private userService:UserService) { }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        if (localStorage.getItem('currentUser')) {
            // logged in so return true
            var curUser= JSON.parse(localStorage.getItem('currentUser'));
            console.log('local user',curUser);
            this.userService.getCurrent().subscribe((user)=>{
                console.log('ngx user',user)
                if(user==null)
                {
                    this.userService.setCurrentUser(curUser);
                }
            })
            return true;
        }
        // not logged in so redirect to login page with the return url
        this.router.navigate(['/login'], { queryParams: { returnUrl: state.url }});
        return false;
    }
}