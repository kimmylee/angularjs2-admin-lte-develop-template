import { User } from '../models/user';
import { Injectable } from '@angular/core';
import { Observable, ReplaySubject } from 'rxjs/Rx';
import { Router } from '@angular/router';

@Injectable()
export class UserService {
    private current: ReplaySubject<User> = new ReplaySubject<User>( 1 );

    constructor(
      private router: Router
    ) {}

    public setCurrent( user: User ) {
      localStorage.setItem('currentUser',JSON.stringify(user)); 
      this.current.next(user);
    }

    public getCurrent() {
      return this.current;
    }

    public getCurrentUser()
    {
      var userData=JSON.parse(localStorage.getItem('currentUser'));
      return userData;
    }

    public logout() {
      localStorage.removeItem('currentUser');
      localStorage.removeItem('access_token');
      const user = new User();
      user.connected = false;
      this.setCurrentUser( user );
      this.router.navigate(['login']);
    }

    // deprecated
    public setCurrentUser(data: any) {
      console.log('start set current user');
      return this.setCurrent(data);
    }
}
