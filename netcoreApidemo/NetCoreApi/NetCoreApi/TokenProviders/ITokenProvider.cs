﻿using Microsoft.IdentityModel.Tokens;
using NetCoreApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NetCoreApi.TokenProviders
{
    public interface ITokenProvider
    {
        string CreateToken(string user, DateTime expiry);

        TokenValidationParameters GetValidationParameters();
    }
}
