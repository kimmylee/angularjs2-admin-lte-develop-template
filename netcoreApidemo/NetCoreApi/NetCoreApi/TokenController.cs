﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using NetCoreApi.TokenProviders;
using NetCoreApi.Models;
using Microsoft.AspNetCore.Identity;
using NetCoreApi.Data;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace NetCoreApi
{

    public class TokenController : Controller
    {
        private ITokenProvider _tokenProvider;
        private readonly UserManager<ApplicationUser> _userManager;
        
        public TokenController(
            ITokenProvider tokenProvider, 
            UserManager<ApplicationUser> userManager) // We'll create this later, don't worry.
        {
           _tokenProvider = tokenProvider;
            _userManager = userManager;
        }
        [Route("api/token")]
        public ActionResult Token( [FromQuery] string username, [FromQuery] string password, [FromQuery] string refresh_token)
        {
            // Authenticate depending on the grant type.
            ApplicationUser  user =  GetUserByCredentials(username, password);
            if (user == null)
                throw new UnauthorizedAccessException("No!");
            int ageInMinutes = 20;  // However long you want...

            DateTime expiry = DateTime.UtcNow.AddMinutes(ageInMinutes);
            var token = new JsonWebToken
            {
                access_token = _tokenProvider.CreateToken(user.UserName,expiry),
                expires_in = ageInMinutes * 60,
            };
            return Ok(new { tokenData=token,userName=user.UserName,email=user.Email});
        }

        //private ApplicationUser GetUserByToken(string refreshToken)
        //{
        //    if (!string.IsNullOrEmpty(refreshToken))
        //    {
        //        var user= _userManager.token
        //    }

        //    // TODO: Check token against your database.
        //    if (refreshToken == "test")
        //        return new User { UserName = "test" };

        //    return null;
        //}

        private ApplicationUser GetUserByCredentials(string username, string password)
        {
            // TODO: Check username/password against your database.
            var dbUser = _userManager.FindByNameAsync(username).Result;
            var isCorrect = _userManager.CheckPasswordAsync(dbUser, password).Result;
            if (isCorrect)
            {
                return dbUser;
                //return new User
                //{
                //    UserName = dbUser.UserName,
                //    Email = dbUser.Email,
                //    AvatarUrl = ""
                //};
            }
            return null;
        }

        private string GenerateRefreshToken(ApplicationUser user)
        {
            var refreshToken = _userManager.GenerateUserTokenAsync(user, "jwt", "test").Result;

            var isSuccess= _userManager.SetAuthenticationTokenAsync(user, "authApi", user.UserName, refreshToken).Result;
            // TODO: Create and persist a refresh token.
            return "refreshToken";
        }
    }
}
