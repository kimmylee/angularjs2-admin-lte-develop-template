﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using NetCoreApi.Data;
using Microsoft.AspNetCore.Identity;

namespace NetCoreApi.Controllers
{
   // [Authorize]
    [Route("api/[controller]")]
    public class AccountController : Controller
    {
        private readonly UserManager<ApplicationUser> _userManager;
        public AccountController(UserManager<ApplicationUser> userManager)
        {
            _userManager = userManager;
        }

        // POST api/values
        [HttpPost]
        public ActionResult Register([FromBody]string email,[FromBody] string password)
        {
            var user = new ApplicationUser { UserName = email, Email = email};
            var result =  _userManager.CreateAsync(user, password).Result;
            if (result.Succeeded)
            {
                return Ok(new { success = true });
            }
            else
            {
                var msg = string.Empty;
                foreach (var error in result.Errors)
                {
                    msg += error.Description;
                }
                return Json(new { success = false, msg = msg });
            }
        }
    }
}
