﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using NetCoreApi.Data;

namespace NetCoreApi.Controllers
{
   // [Authorize]
    [Route("api/[controller]")]
    public class HeroController : Controller
    {
        // GET api/values
        [HttpGet]
        public JsonResult  Get()
        {
            var heroes = GetHeroes();
            return Json(heroes);
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public string Get(int id)
        {
            return "value";
        }

        // POST api/values
        [HttpPost]
        public JsonResult Post(string name)
        {
            var hero = new Hero { Id = DateTime.Now.Ticks, Name = name };
            return Json(hero);
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }


        private List<Hero> GetHeroes()
        {
            return new List<Hero> {
                new Hero{ Id=1,Name="Zero"},
                new Hero{ Id=11,Name="Lee"},
                new Hero{ Id=1,Name="Rabbit"},
            };
        }
    }
}
