﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NetCoreApi.Data
{
    public class Hero
    {
        public long Id { get; set; }
        public string Name { get; set; }
    }
}
